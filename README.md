# Open Container Initiative OCI

Open standards for operating-system-level virtualization. https://en.m.wikipedia.org/wiki/Open_Container_Initiative

This contains [entrypoint](https://gitlab.com/notes-on-computer-communication-protocols/entrypoint) and [entrypoint-env](https://gitlab.com/notes-on-computer-communication-protocols/entrypoint-env) which is used by GitLab to start a shell.



![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=docker+podman+buildah+skopeo&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)

# Official documentation
* [Open Container Initiative](https://opencontainers.org)

# Semi official documentation
## Docker Blog
* [*Demystifying the Open Container Initiative (OCI) Specifications*
  ](https://www.docker.com/blog/demystifying-open-container-initiative-oci-specifications/)
  2017-07 Stephen Walli

## Red Hat Customer Portal
### Building, running, and managing Linux containers on Red Hat Enterprise Linux 8
Red Hat Customer Content Services
* [*Chapter 1. Starting with containers*
  ](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/building_running_and_managing_containers/starting-with-containers_building-running-and-managing-containers)
* [*Chapter 8. Container command-line reference*
  ](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/building_running_and_managing_containers/container-command-line-reference_building-running-and-managing-containers)

# Unofficial documentation
* [OCI container images](https://google.com/search?q=OCI+container+images)
* [*Open Container Initiative*](https://en.wikipedia.org/wiki/Open_Container_Initiative)
  WikipediA
* [*Docker and OCI Runtimes*
  ](https://medium.com/@avijitsarkar123/docker-and-oci-runtimes-a9c23a5646d6)
  2019-11 Avijit Sarkar

# Tools to build OCI images
* buildah and podman
* docker
* [img](https://github.com/genuinetools/img)
* ...
* [*The Many Ways to Build an OCI Image without Docker*
  ](https://www.projectatomic.io/blog/2018/03/the-many-ways-to-build-oci-images/)
  2018-03 Micah Abbott